package cz.esw.serialization.json;

/**
 * @author Marek Cuchý
 */
public enum DataType {
    DOWNLOAD(0),
    UPLOAD(1),
    PING(2);

    private final int enumNumber;

    DataType(int enumNumber) {
        this.enumNumber = enumNumber;
    }

    public int getEnumNumber() {
        return enumNumber;
    }

    public static DataType getDataType(int enumNumber) {
        return switch (enumNumber) {
            case 0 -> DOWNLOAD;
            case 1 -> UPLOAD;
            case 2 -> PING;
            default -> PING;
        };
    }
}
