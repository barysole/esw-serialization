package cz.esw.serialization.handler;

import cz.esw.serialization.ResultConsumer;
import cz.esw.serialization.json.DataType;
import cz.esw.serialization.json.Dataset;
import cz.esw.serialization.json.MeasurementInfo;
import cz.esw.serialization.proto.*;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

/**
 * @author Marek Cuchý (CVUT)
 */
public class ProtoDataHandler implements DataHandler {

    private final InputStream inputStream;
    private final OutputStream outputStream;
    protected Map<Integer, Dataset> datasets;

    public ProtoDataHandler(InputStream is, OutputStream os) {
        this.inputStream = is;
        this.outputStream = os;
    }

    @Override
    public void initialize() {
        datasets = new HashMap<>();
    }

    @Override
    public void handleNewDataset(int datasetId, long timestamp, String measurerName) {
        MeasurementInfo info = new MeasurementInfo(datasetId, timestamp, measurerName);
        Map<DataType, List<Double>> recordMap = new EnumMap<>(DataType.class);

        datasets.put(datasetId, new Dataset(info, recordMap));
    }

    @Override
    public void handleValue(int datasetId, DataType type, double value) {
        Dataset dataset = datasets.get(datasetId);
        if (dataset == null) {
            throw new IllegalArgumentException("Dataset with id " + datasetId + " not initialized.");
        }
        dataset.getRecords().computeIfAbsent(type, t -> new ArrayList<>()).add(value);
    }

    @Override
    public void getResults(ResultConsumer consumer) throws IOException {
        ProtoRepeatedMRecordsDataset.Builder datasetsWithRecords = ProtoRepeatedMRecordsDataset.newBuilder();
        for (Dataset dataset : datasets.values()) {
            ProtoMRecordsDataset.Builder datasetRB = ProtoMRecordsDataset.newBuilder();
            dataset.getRecords().forEach((dataType, doubles) -> datasetRB.putRecordsWithType(dataType.getEnumNumber(), ProtoMeasurementRecords.newBuilder().addAllRecords(doubles).build()));
            datasetsWithRecords.addDatasets(datasetRB.setMeasurementInfo(ProtoMeasurementInfo.newBuilder()
                            .setId(dataset.getInfo().id())
                            .setTimestamp(dataset.getInfo().timestamp())
                            .setMeasurerName(dataset.getInfo().measurerName()))
                    .build()
            );
        }
        ProtoRepeatedMRecordsDataset datasetsWithRecordsReq = datasetsWithRecords.build();

        //send message size
        int messageSize = datasetsWithRecordsReq.getSerializedSize();
        new DataOutputStream(outputStream).writeInt(messageSize);

        datasetsWithRecordsReq.writeTo(outputStream);

        ProtoRepeatedMAvgDataset repeatedMAvgDataset = ProtoRepeatedMAvgDataset.parseFrom(inputStream);

        for (ProtoMAvgDataset result : repeatedMAvgDataset.getDatasetsList()) {
            ProtoMeasurementInfo info = result.getMeasurementInfo();
            consumer.acceptMeasurementInfo(info.getId(), info.getTimestamp(), info.getMeasurerName());
            result.getAvgRecordsWithTypeMap().forEach((type, avgResult) -> consumer.acceptResult(DataType.getDataType(Integer.parseInt(type.toString())), avgResult));
        }
    }
}
