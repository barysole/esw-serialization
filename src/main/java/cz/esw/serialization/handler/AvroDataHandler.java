package cz.esw.serialization.handler;

import cz.esw.serialization.ResultConsumer;
import cz.esw.serialization.avro.*;
import cz.esw.serialization.json.DataType;
import cz.esw.serialization.json.Dataset;
import cz.esw.serialization.json.MeasurementInfo;
import org.apache.avro.io.*;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;

import java.io.*;
import java.util.*;

/**
 * @author Marek Cuchý (CVUT)
 */
public class AvroDataHandler implements DataHandler {

    private final InputStream inputStream;
    private final OutputStream outputStream;

    protected Map<Integer, Dataset> datasets;
    private final EncoderFactory encoderFactory = new EncoderFactory();
    private final DecoderFactory decoderFactory = new DecoderFactory();
    private final DatumReader<AvroMAvgDatasetResponse> avroMAvgDatasetResponseDR = new SpecificDatumReader<>(AvroMAvgDatasetResponse.class);
    private final DatumWriter<AvroMAvgDatasetRequest> avroMAvgDatasetRequestDW = new SpecificDatumWriter<>(AvroMAvgDatasetRequest.class);

    public AvroDataHandler(InputStream is, OutputStream os) {
        this.inputStream = is;
        this.outputStream = os;
    }

    @Override
    public void initialize() {
        datasets = new HashMap<>();
    }

    //todo: might avro classes uses here ?
    @Override
    public void handleNewDataset(int datasetId, long timestamp, String measurerName) {
        MeasurementInfo info = new MeasurementInfo(datasetId, timestamp, measurerName);
        Map<DataType, List<Double>> recordMap = new EnumMap<>(DataType.class);

        datasets.put(datasetId, new Dataset(info, recordMap));
    }

    @Override
    public void handleValue(int datasetId, DataType type, double value) {
        Dataset dataset = datasets.get(datasetId);
        if (dataset == null) {
            throw new IllegalArgumentException("Dataset with id " + datasetId + " not initialized.");
        }
        dataset.getRecords().computeIfAbsent(type, t -> new ArrayList<>()).add(value);
    }

    @Override
    public void getResults(ResultConsumer consumer) throws IOException {
        AvroMAvgDatasetRequest avroMAvgDatasetRequest = new AvroMAvgDatasetRequest();
        List<AvroMRecordsDataset> avroMRecordsDatasetList = new ArrayList<>(datasets.size());
        for (Dataset dataset : datasets.values()) {
            AvroMRecordsDataset avroMRecordsDataset = new AvroMRecordsDataset();
            avroMRecordsDataset.setRecordsWithType(new HashMap<>());
            dataset.getRecords().forEach((dataType, doubles) -> avroMRecordsDataset.getRecordsWithType().put(String.valueOf(dataType.getEnumNumber()), doubles));
            avroMRecordsDataset.setMeasurementInfo(AvroMeasurementInfo.newBuilder()
                    .setId(dataset.getInfo().id())
                    .setTimestamp(dataset.getInfo().timestamp())
                    .setMeasurerName(dataset.getInfo().measurerName())
                    .build());
            avroMRecordsDatasetList.add(avroMRecordsDataset);
        }
        avroMAvgDatasetRequest.setContent(avroMRecordsDatasetList);

        //send message size
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        BinaryEncoder encoder = encoderFactory.binaryEncoder(byteArrayOutputStream, null);
        avroMAvgDatasetRequestDW.write(avroMAvgDatasetRequest, encoder);
        encoder.flush();
        int messageSize = byteArrayOutputStream.size();
        new DataOutputStream(outputStream).writeInt(messageSize);

        //send message
        BinaryEncoder binaryEncoder = encoderFactory.binaryEncoder(outputStream, null);
        avroMAvgDatasetRequestDW.write(avroMAvgDatasetRequest, binaryEncoder);
        binaryEncoder.flush();

        //receive message
        AvroMAvgDatasetResponse avroMAvgDatasetResponse = new AvroMAvgDatasetResponse();
        BinaryDecoder binaryDecoder = decoderFactory.binaryDecoder(inputStream, null);
        avroMAvgDatasetResponseDR.read(avroMAvgDatasetResponse, binaryDecoder);

        for (AvroMAvgDataset result : avroMAvgDatasetResponse.getContent()) {
            AvroMeasurementInfo info = result.getMeasurementInfo();
            consumer.acceptMeasurementInfo(info.getId(), info.getTimestamp(), info.getMeasurerName().toString());
            result.getAvgRecordsWithType().forEach((type, avgResult) -> consumer.acceptResult(DataType.getDataType(Integer.parseInt(type.toString())), avgResult));
        }
    }
}
