#include <iostream>

#include <jsoncpp/json/json.h>

#include "dataset.h"
#include "result.h"

#include <boost/asio.hpp>
#include <boost/algorithm/string.hpp>

#include "measurements.pb.h"
#include "Ameasurements.hh"

using namespace std;
using boost::asio::ip::tcp;

void processJSON(tcp::iostream &stream) {
    Json::Value val;
    Json::Reader reader;

    std::vector<Dataset> datasets;
    std::vector<Result> results;

    /* Read json string from the stream */
    string s;
    getline(stream, s, '\0');

    /* Parse string */
    reader.parse(s, val);

    datasets.clear();
    results.clear();
    for (int i = 0; i < val.size(); i++) {
        datasets.emplace_back();
        datasets[i].Deserialize(val[i]);
        /* Calculate averages */
        results.emplace_back(datasets[i].getInfo(), datasets[i].getRecords());
    }

    /* Create output JSON structure */
    Json::Value out;
//    Json::FastWriter writer;
    Json::StyledWriter writer;
    for (int i = 0; i < results.size(); i++) {
        Json::Value result;
        results[i].Serialize(result);
        out[i] = result;
    }

    /* Send the result back */
    std::string output = writer.write(out);
    stream << output;
    cout << output;
}


int getSizeOfData(tcp::iostream &stream) {
    char buf[4];
    char convert[4];
    int value;

    stream.read(buf, 4);
    convert[0] = buf[3];
    convert[1] = buf[2];
    convert[2] = buf[1];
    convert[3] = buf[0];

    std::memcpy(&value, convert, sizeof(int));
    return value;
}

void processAvro(tcp::iostream &stream) {
    cout << "Handling avro" << endl;
    int dataSize = getSizeOfData(stream);
    char *buffer = new char[dataSize];
    stream.read(buffer, dataSize);

    std::unique_ptr<avro::InputStream> inStream = avro::memoryInputStream((uint8_t *) buffer, dataSize);
    avro::DecoderPtr decoder = avro::binaryDecoder();
    decoder->init(*inStream);

    c::AvroMAvgDatasetRequest request;
    avro::decode(*decoder, request);
    c::AvroMAvgDatasetResponse avgDatasetResponse;


    for (int i = 0; i < request.content.size(); i++) {
        c::AvroMRecordsDataset avroMRecordsDataset = request.content[i];
        c::AvroMAvgDataset avgDataset;
        avgDataset.measurementInfo = avroMRecordsDataset.measurementInfo;

        map<string, double> averages;
        auto records = avroMRecordsDataset.recordsWithType;
        for (auto record: records) {
            double sum = 0;
            double count = 0;
            auto data = record.second;
            for (auto item: data) {
                count++;
                sum += item;
            }
            double avg = sum / count;
            auto cat = record.first;
            averages.insert({record.first, avg});
        }
        avgDataset.avgRecordsWithType = averages;

        avgDatasetResponse.content.push_back(avgDataset);
    }

    std::unique_ptr<avro::OutputStream> outStream = avro::ostreamOutputStream(stream);
    avro::EncoderPtr encoder = avro::binaryEncoder();
    encoder->init(*outStream);

    avro::encode(*encoder, avgDatasetResponse);
    encoder->flush();
    delete[]buffer;
}

esw::ProtoRepeatedMAvgDataset protoInfoList(const esw::ProtoRepeatedMRecordsDataset &dataSet) {
    esw::ProtoRepeatedMAvgDataset response;

    for (int i = 0; i < dataSet.datasets_size(); i++) {
        esw::ProtoMRecordsDataset recordDataset = dataSet.datasets(i);
        esw::ProtoMeasurementInfo measurementInfo = recordDataset.measurementinfo();
        esw::ProtoMAvgDataset mAvgDataset;
        *mAvgDataset.mutable_measurementinfo() = measurementInfo;
        google::protobuf::Map<int, double> averages;

        auto times = recordDataset.recordswithtype();
        for (auto item: times) {
            double sum = 0;
            double count = 0;
            for (auto record: item.second.records()) {
                sum += record;
                count++;
            }
            double avg = sum / count;
            averages.insert({item.first, avg});
        }
        *mAvgDataset.mutable_avgrecordswithtype() = averages;
        response.add_datasets()->CopyFrom(mAvgDataset);
    }

    return response;
}


void processProtobuf(tcp::iostream &stream) {
    cout << "Handling proto" << endl;

    int dataSize = getSizeOfData(stream);
    std::cout << std::dec << dataSize << '\n';

    char *buffer = new char[dataSize];
    stream.read(buffer, dataSize);

    esw::ProtoRepeatedMRecordsDataset info;
    info.ParseFromArray(buffer, dataSize);
    esw::ProtoRepeatedMAvgDataset response = protoInfoList(info);

    //print result
//    for (int i = 0; i < response.datasets_size(); i++) {
//        esw::ProtoMAvgDataset recordDataset = response.datasets(i);
//        cout << "{" << endl;
//        cout << "timestamp: " << recordDataset.measurementinfo().timestamp() << ", name: "
//             << recordDataset.measurementinfo().measurername() << ", id: " << recordDataset.measurementinfo().id()
//             << endl;
//        auto times = recordDataset.avgrecordswithtype();
//        cout << "   [" << endl;
//        for (auto item: times) {
//            cout << "       " << item.first << ": " << item.second << endl;
//        }
//        cout << "   ]" << endl;
//        cout << "}" << endl;
//    }

    std::string output = response.SerializeAsString();
    stream << output;
    delete[] buffer;
}

int main(int argc, char *argv[]) {

    if (argc != 3) {
        cout << "Error: two arguments required - ./server  <port> <protocol>" << endl;
        return 1;
    }

    // unsigned short int port = 12345;
    unsigned short int port = atoi(argv[1]);

    // std::string protocol = "json";
    std::string protocol(argv[2]);
    boost::to_upper(protocol);
    try {
        boost::asio::io_service io_service;

        tcp::endpoint endpoint(tcp::v4(), port);
        tcp::acceptor acceptor(io_service, endpoint);

        while (true) {
            cout << "Waiting for message in " + protocol + " format..." << endl;
            tcp::iostream stream;
            boost::system::error_code ec;
            acceptor.accept(*stream.rdbuf(), ec);

            if (protocol == "JSON") {
                processJSON(stream);
            } else if (protocol == "AVRO") {
                processAvro(stream);
            } else if (protocol == "PROTO") {
                processProtobuf(stream);
            } else {
                throw std::logic_error("Protocol not yet implemented");
            }

        }

    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}
